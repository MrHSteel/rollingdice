const count = [0,0,0,0,0,0,0,0,0,0,0]

function rollDice(){
    var d1 = Math.floor(Math.random() * 6) + 1;
    var d2 = Math.floor(Math.random() * 6) + 1;
    return d1 + d2 
}
function rollCount(numRolls){
    for(let i = 0; i <=numRolls; i++){
        const dice_roll = rollDice()
        count[dice_roll-2] ++
    }
    return count
}
console.log(rollCount(1000))

function barTable() {
    let dest_graph = document.getElementById("totals")

    for(let i=0;i<count.length; i++){
        let bar = document.createElement("div")
        bar.style.height = "20px"
        bar.style.width = count[i]+"px"
        bar.style.backgroundColor = "lightblue"
        bar.style.margin = "10px"
       
        let sum_text = document.createTextNode(i+2 + ':' + count[i]+ ' ')
        bar.appendChild(sum_text)
        dest_graph.appendChild(bar)
    }
}
rollCount (1000)
barTable()
